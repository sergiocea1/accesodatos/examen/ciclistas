<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    // Ejercicio 2
    public function actionEjercicio2()
    {
      $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nombre), ciclista.dorsal FROM ciclista 
                   INNER JOIN etapa ON  ciclista.dorsal = etapa.dorsal
                   INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal
                   INNER JOIN maillot ON lleva.código = maillot.código
                   WHERE color != "rosa"')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT DISTINCT nombre, ciclista.dorsal FROM ciclista 
                      INNER JOIN etapa ON  ciclista.dorsal = etapa.dorsal
                      INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal
                      INNER JOIN maillot ON lleva.código = maillot.código
                      WHERE color != "rosa"',
            'totalCount' => $numero,
            
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Ejercicio 2",
            "explicacion"=>"Selecciono el nombre y dorsal de los ciclista ya que es lo que me pide que muestre luego realizo los INNER JOIN para ver los ciclista que existen en todas las tablas y luego los filtros por los que nunca han llevado el maillot rosa ",
            "sql"=>"SELECT DISTINCT nombre, ciclista.dorsal FROM ciclista 
                    INNER JOIN etapa ON  ciclista.dorsal = etapa.dorsal
                    INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal
                    INNER JOIN maillot ON lleva.código = maillot.código
                    WHERE color != 'rosa'",
        ]);  
    }
    // Ejercicio 3
    public function actionEjercicio3(){
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select('SUM(Kms) as Kilometros'),
            
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['Kilometros'],
            "titulo"=>"Ejercicio 3",
            "explicacion"=>"Sumo los Kms que tiene cada etapa y me devuelve el total de todos ellos y esto lo guardo en una variable llamada Kilometros para luego mostrarla",
            "sql"=>"SELECT SUM(kms) FROM etapa",

        ]);
    }

}
